use super::{Imgs, Message};
use std::{path::PathBuf, sync::Arc};

use crate::ui::{Graphic, fonts::IcedFont as Font, fonts::IcedFonts as Fonts, 
    ice::{Element, IcedRenderer, IcedUi as Ui, Id, component::neat_button, style, widget::{
        Image, compound_graphic, compound_graphic::CompoundGraphic, BackgroundContainer
    }}
};

use common::assets::{self, AssetExt};
use i18n::Localization;
use iced::{button, Button, Align, Column, Container, Scrollable, scrollable, Length, Padding, Row, Space, Text};
use tracing::warn;
use vek::Rgba;
use world::sim::WorldSimMetadata;
use ::image::io::Reader as ImageReader;

pub struct Screen {
    cancel_button: button::State,
    generate_button: button::State,
    start_button: button::State,
    map_list: scrollable::State,
    map_list_entries: Vec<button::State>,
    map_md: Option<(WorldSimMetadata, Option<Id>)>,
    map_img_missing_img: Id
}

impl Screen {
    pub fn new(ui: &mut Ui, map_list_size: usize) -> Self {
        let map_list_entries = (0..map_list_size)
            .map(|_| button::State::default())
            .collect();
        
        let map_img_missing_img = ui.add_graphic(Graphic::Image(
            assets::Image::load("voxygen.element.not_found")
                .unwrap_or_else(|_| panic!("Missing map image placeholder asset"))
                .read()
                .to_image(),
            None,
        ));

        Self {
            cancel_button: Default::default(),
            generate_button: Default::default(),
            start_button: Default::default(),
            map_list: Default::default(),
            map_list_entries,
            map_md: None,
            map_img_missing_img,
        }
    }

    pub fn view_metadata_attribute<'a, M: 'a, R: 'a + iced::text::Renderer + iced::space::Renderer>(
        font: &Font,
        attribute_name: &str,
        attribute_value: &str
    ) -> Row<'a, M, R> {
        Row::with_children(vec![
            Space::new(Length::FillPortion(5), Length::Units(0)).into(),
            Text::new(attribute_name)
                .width(Length::FillPortion(40))
                .size(font.scale(25))
                .horizontal_alignment(iced::HorizontalAlignment::Left)
                .into(),
            Text::new(attribute_value)
                .width(Length::FillPortion(40))
                .size(font.scale(25))
                .horizontal_alignment(iced::HorizontalAlignment::Right)
                .into(),
        ])
    }

    fn map_list_button<'a>(
        btn_state: &'a mut button::State,
        font: &Font,
        imgs: &Imgs,
        picked_map: &PathBuf,
        map_name: &str,
        map_path: &PathBuf
    ) -> Button<'a, Message, IcedRenderer> {
        let color = if picked_map == map_path {
            (97, 255, 18)
        } else {
            (97, 97, 25)
        };
        
        Button::new(
            btn_state,
            Row::with_children(vec![
                Space::new(Length::FillPortion(5), Length::Units(0)).into(),
                Text::new(map_name)
                    .width(Length::FillPortion(95))
                    .size(font.scale(25))
                    .vertical_alignment(iced::VerticalAlignment::Center)
                    .into(),
            ]),
        )
            .style(
                style::button::Style::new(imgs.selection)
                    .hover_image(imgs.selection_hover)
                    .press_image(imgs.selection_press)
                    .image_color(vek::Rgba::new(color.0, color.1, color.2, 192)),
            )
            .on_press(Message::PickSingleplayerMap(map_path.clone()))
    }

    pub fn pick_map(
        &mut self,
        ui: &mut Ui,
        current_map: &PathBuf
    ) {
        let prev_id = self.map_md.as_ref().map(|md| md.1).flatten();
        let map_md_attributes = WorldSimMetadata::md_lookup(current_map)
            .expect("Could not read metadata for default map");
        let mut map_img_path = current_map.clone();
        map_img_path.set_extension("png");
        let map_img_data = ImageReader::open(map_img_path)
            .or_else(|e| {
                warn!(?e, "Could not load map image");
                Err(e)
            }).map(|img_file| img_file
                .decode()
                .or_else(|e| {
                    warn!(?e, "Could not decode map image");
                    Err(e)
                })
                .map(|img| Arc::new(img))
            );
        let map_img_id = match (prev_id, map_img_data) {
            (Some(prev_id), Ok(Ok(map_img))) => {
                ui.replace_graphic(prev_id, Graphic::Image(map_img, None));
                Some(prev_id)
            },
            (None, Ok(Ok(map_img))) => Some(ui.add_graphic(Graphic::Image(map_img, None))),
            _ => None
        };

        self.map_md = Some((map_md_attributes, map_img_id));
    }

    pub(super) fn view(
        &mut self,
        fonts: &Fonts,
        imgs: &Imgs,
        button_style: style::button::Style,
        maps: &Vec<PathBuf>,
        current_map: &PathBuf,
        i18n: &Localization,
    ) -> Element<Message> {
        let mut map_list = Scrollable::new(&mut self.map_list)
            .spacing(8)
            .height(Length::Fill)
            .width(Length::Fill)
            .align_items(Align::Start)
            .padding(Padding::new(8));

        for (map_path, btn_state) in maps.iter().zip(self.map_list_entries.iter_mut()) {
            let map_name = map_path
                .file_stem()
                .expect("No map name provided")
                .to_str()
                .expect("Can't turn map file name into string");

            map_list = map_list.push(Self::map_list_button(
                btn_state,
                &fonts.cyri,
                imgs,
                current_map,
                map_name,
                map_path
            ));
        }

        let map_list_decorated = BackgroundContainer::new(
            CompoundGraphic::from_graphics(vec![
                compound_graphic::Graphic::rect(Rgba::new(0, 0, 0, 240), [300, 500], [0, 0])
            ]), map_list);

        let map_list_decorated = Container::new(map_list_decorated)
            .height(Length::Fill)
            .width(Length::FillPortion(60));

        let (current_map_md, map_img_id) = match &self.map_md {
            Some((m, Some(i))) => (Some(m), Some(i)),
            Some((m, None)) => (Some(m), None),
            _ => (None, None)
        };

        let md_attribute_column: Element<_> = match current_map_md {
            Some(current_map_md) => {
                let map_metadata_ver = Self::view_metadata_attribute(
                    &fonts.cyri,
                    "Version",
                    &current_map_md.ver
                );
        
                let map_metadata_size_x = Self::view_metadata_attribute(
                    &fonts.cyri, 
                    "Map size X",
                    &current_map_md.map_size_lg.0[0].to_string()
                );
        
                let map_metadata_size_y = Self::view_metadata_attribute(
                    &fonts.cyri, 
                    "Map size Y",
                    &current_map_md.map_size_lg.0[1].to_string()
                );

                Column::with_children(vec![
                    map_metadata_ver.into(),
                    map_metadata_size_x.into(),
                    map_metadata_size_y.into()
                ])
                    .height(Length::FillPortion(30))
                    .width(Length::Fill)
                    .align_items(Align::Start)
                    .into()
            },
            None => {
                Space::new(Length::Fill, Length::FillPortion(30))
                    .into()
            }
        };

        let md_attributes_decorated = BackgroundContainer::new(
            CompoundGraphic::from_graphics(vec![
                compound_graphic::Graphic::rect(Rgba::new(0, 0, 0, 240), [300, 500], [0, 0])
            ]),
            md_attribute_column
        );

        let md_img = match map_img_id {
            Some(map_img_id) => Image::new(*map_img_id),
            None => Image::new(self.map_img_missing_img)
        };

        let md_img = md_img
            .height(Length::Units(256))
            .width(Length::Units(256))
            .into();

        let md_column = Column::with_children(vec![
            md_img,
            md_attributes_decorated.into()
        ])
            .width(Length::FillPortion(40))
            .height(Length::Fill)
            .align_items(Align::Center);

        let content_space_1 = Space::new(Length::FillPortion(5), Length::Fill);
        let content_space_2 = Space::new(Length::FillPortion(5), Length::Fill);

        let top_content_row = Row::with_children(vec![
            content_space_1.into(),
            map_list_decorated.into(),
            md_column.into(),
            content_space_2.into()
        ])
            .spacing(8)
            .width(Length::Fill)
            .height(Length::FillPortion(95))
            .align_items(Align::Center);

        let cancel = Container::new(neat_button(
            &mut self.cancel_button,
            i18n.get("common.cancel"),
            0.7,
            button_style,
            Some(Message::Back)
        ));
        let gen = Container::new(neat_button(
            &mut self.generate_button,
            i18n.get("common.map_gen"),
            0.7,
            button_style,
            None
        ));
        let start = Container::new(neat_button(
            &mut self.start_button,
            i18n.get("common.map_start"),
            0.7,
            button_style,
            Some(Message::SingleplayerStart)
        ));
        let bottom_button_row = Row::with_children(vec![cancel.into(), gen.into(), start.into()])
            .spacing(8)
            .width(Length::Fill)
            .height(Length::FillPortion(5))
            .align_items(Align::Center);

        Column::with_children(vec![
            top_content_row.into(),
            bottom_button_row.into()
        ])
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }
}