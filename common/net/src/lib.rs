#![allow(incomplete_features)]
#![feature(
    const_generics,
    const_evaluatable_checked,
    const_fn_floating_point_arithmetic
)]
pub mod msg;
pub mod sync;
